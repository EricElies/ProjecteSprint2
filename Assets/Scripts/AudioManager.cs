﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable()]
public struct SoundParameters
{
    public float Volume;
    public float Pitch;
    public bool Loop;
}
[System.Serializable()]
public class Sound
{
    //[SerializeField] string name;
    public string Name;// { get { return name; } }

    //[SerializeField] AudioClip clip;
    public AudioClip Clip; //{ get { return clip; } }

    // [SerializeField] SoundParameters parameters;
    public SoundParameters Parameters;// { get { return parameters; } }

    
    public AudioSource Source; 


    public void Play()
    {
        /*Debug.Log("0");
        Source.clip = Clip;
        Debug.Log("1");
        Source.volume = Parameters.Volume;
        Debug.Log("2");
        Source.pitch = Parameters.Pitch;
        Debug.Log("3");
        Source.loop = Parameters.Loop;
        Debug.Log("4");
        Source.Play();
        Debug.Log("5");*/
        Source.PlayOneShot(Clip, Parameters.Volume);
    }

    public void Stop()
    {
        Source.Stop();

    }
}
public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField] Sound[] sounds;
    //[SerializeField] AudioSource sourcePrefab;
    //[SerializeField] string startupTrack;

    void Start()
    {
        if (instance != null)
        { Destroy(gameObject); }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        //InitSounds();
        /*if (string.IsNullOrEmpty(startupTrack) != true)
        {
            PlaySound(startupTrack);
        }*/
    }

    /*void InitSounds()
    {
        foreach (var sound in sounds)
        {
            AudioSource source = (AudioSource)Instantiate(sourcePrefab, gameObject.transform);
            source.name = sound.Name;
            sound.Source = source;
        }
    }
  */

    public void PlaySound(string name)
    {
        var sound = GetSound(name);
        if (sound != null)
        {
            sound.Play();
        }
        else
        {
            Debug.LogWarning("Sound by the name " + name + " not found. Issues occured at AudioManager.PlaySound()");
        }
    }

    /*public void StopSound(string name)
    {
        var sound = GetSound(name);
        if (sound != null)
        {
            sound.Stop();
        }
        else
        {
            Debug.LogWarning("Sound by the name " + name + " not found. Issues occured at AudioManager.StopSound()");
        }
    }*/

    Sound GetSound(string name)
    {
        foreach (var sound in sounds)
        {
            if(sound.Name.Equals(name))
            {
                return sound;
            }
        }
        return null;
    }
    

}
