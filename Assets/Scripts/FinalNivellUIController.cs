﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalNivellUIController : MonoBehaviour
{
    public Text Nivell;
    public Text Puntuacio;
    public Button NextLevel;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initFinalNivell();
        Nivell.text = GameManager.Instance.AvariableLevels[GameManager.Instance.Nivell].text;
        Puntuacio.text = (GameManager.Instance.Puntuacions[GameManager.Instance.Nivell] + "%");
        if (GameManager.Instance.AvariableLevels.Count-1 <= GameManager.Instance.Nivell)
        {
            //NextLevel.enabled = false;
            NextLevel.gameObject.SetActive(false);
        }
    }
    public void retornMenu()
    {
        GameManager.Instance.ToMenu();
    }
    public void seguentNivell()
    {
        GameManager.Instance.NextLevel();
    }
    public void repetirNivell()
    {
        GameManager.Instance.ToPlay(GameManager.Instance.Nivell);
    }
}
