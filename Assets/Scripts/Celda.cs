﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Celda : MonoBehaviour
{
    public Color ColorPrincipal, ColorSolucion;
    public bool solucion;
    public SpriteRenderer imatge;
    public float dificultat;
    
    public float rangoDificultatX = 0.70f;
    public float rangoDificultatY = 0.85f;
    public float dificultatSolucio;
    

    public void init()
    {
        dificultat = Random.Range(rangoDificultatX, rangoDificultatY); 
        switch (GameManager.Instance.Nivell) 
        { 
            case 0: 
        
            ColorPrincipal = new Color(0, 0, 0, Random.Range(0.8f, 1f)); break;
            case 1:
                switch (GameManager.Instance.Ronda)
                {
                    case 10:
                        dificultat += 0.005f; break;
                    case 15:
                        dificultat += 0.010f; break;
                    case 20:
                        dificultat += 0.015f; break;
                    case 25:
                        dificultat += 0.018f; break;
                    case 30:
                        dificultat += 0.02f; break;
                    case 35:
                        dificultat += 0.022f; break;
                    case 40:
                        dificultat += 0.024f; break;
                    case 45:
                        dificultat += 0.026f; break;
                    case 50:
                        dificultat += 0.028f; break;
                    case 55:
                        dificultat += 0.030f; break;
                    case 60:
                        dificultat += 0.032f; break;
                    case 65:
                        dificultat += 0.034f; break;
                    case 70:
                        dificultat += 0.036f; break;
                    case 75:
                        dificultat += 0.038f; break;
                    case 80:
                        dificultat += 0.04f; break;
                    case 85:
                        dificultat += 0.042f; break;
                    case 90:
                        dificultat += 0.044f; break;
                    case 95:
                        dificultat += 0.046f; break;
                    case 100:
                        dificultat += 0.048f; break;
                }

                  ColorPrincipal = new Color(dificultat, 0, 0, 1);

                break;
            case 2:
                switch (GameManager.Instance.Ronda)
                {
                    case 10:
                        dificultat += 0.005f; break;
                    case 15:
                        dificultat += 0.010f; break;
                    case 20:
                        dificultat += 0.015f; break;
                    case 25:
                        dificultat += 0.018f; break;
                    case 30:
                        dificultat += 0.02f; break;
                    case 35:
                        dificultat += 0.022f; break;
                    case 40:
                        dificultat += 0.024f; break;
                    case 45:
                        dificultat += 0.026f; break;
                    case 50:
                        dificultat += 0.028f; break;
                    case 55:
                        dificultat += 0.030f; break;
                    case 60:
                        dificultat += 0.032f; break;
                    case 65:
                        dificultat += 0.034f; break;
                    case 70:
                        dificultat += 0.036f; break;
                    case 75:
                        dificultat += 0.038f; break;
                    case 80:
                        dificultat += 0.04f; break;
                    case 85:
                        dificultat += 0.042f; break;
                    case 90:
                        dificultat += 0.044f; break;
                    case 95:
                        dificultat += 0.046f; break;
                    case 100:
                        dificultat += 0.048f; break;
                }
                ColorPrincipal = new Color(0, 0, dificultat, 1);
                break;
            case 3:
                switch (GameManager.Instance.Ronda)
                {
                    case 10:
                        dificultat += 0.005f; break;
                    case 15:
                        dificultat += 0.010f; break;
                    case 20:
                        dificultat += 0.015f; break;
                    case 25:
                        dificultat += 0.018f; break;
                    case 30:
                        dificultat += 0.02f; break;
                    case 35:
                        dificultat += 0.022f; break;
                    case 40:
                        dificultat += 0.024f; break;
                    case 45:
                        dificultat += 0.026f; break;
                    case 50:
                        dificultat += 0.028f; break;
                    case 55:
                        dificultat += 0.030f; break;
                    case 60:
                        dificultat += 0.032f; break;
                    case 65:
                        dificultat += 0.034f; break;
                    case 70:
                        dificultat += 0.036f; break;
                    case 75:
                        dificultat += 0.038f; break;
                    case 80:
                        dificultat += 0.04f; break;
                    case 85:
                        dificultat += 0.042f; break;
                    case 90:
                        dificultat += 0.044f; break;
                    case 95:
                        dificultat += 0.046f; break;
                    case 100:
                        dificultat += 0.048f; break;
                }
                ColorPrincipal = new Color(0, dificultat, 0, 1);
                break;
        }
        imatge.color = ColorPrincipal;
        solucion = false;
    }

        /*if (GameManager.Instance.Nivell == 1)
        {
            switch (GameManager.Instance.Ronda)
            {
                case 10:
                    dificultat += 0.005f; break; 
                case 15:
                    dificultat += 0.010f; break;
                case 20:
                    dificultat += 0.015f; break;
                case 25:
                    dificultat += 0.018f; break;
                case 30:
                    dificultat += 0.02f;  break;
                case 35:
                    dificultat += 0.022f; break;
                case 40:
                    dificultat += 0.024f; break;
                case 45:
                    dificultat += 0.026f; break;
                case 50:
                    dificultat += 0.028f; break;
                case 55:
                    dificultat += 0.030f; break;
                case 60:
                    dificultat += 0.032f; break; 
                case 65:
                    dificultat += 0.034f; break;
                case 70:
                    dificultat += 0.036f; break;
                case 75:
                    dificultat += 0.038f; break;
                case 80:
                    dificultat += 0.04f; break;
                case 85:
                    dificultat += 0.042f; break;
                case 90:
                    dificultat += 0.044f; break;
                case 95:
                    dificultat += 0.046f; break;
                case 100:
                    dificultat += 0.048f; break;
            }

            ColorPrincipal = new Color(dificultat, 0, 0, 1);


        }
        if (GameManager.Instance.Nivell == 2)
        {
            switch (GameManager.Instance.Ronda)
            {
                case 10:
                    dificultat += 0.005f; break;
                case 15:
                    dificultat += 0.010f; break;
                case 20:
                    dificultat += 0.015f; break;
                case 25:
                    dificultat += 0.018f; break;
                case 30:
                    dificultat += 0.02f; break;
                case 35:
                    dificultat += 0.022f; break;
                case 40:
                    dificultat += 0.024f; break;
                case 45:
                    dificultat += 0.026f; break;
                case 50:
                    dificultat += 0.028f; break;
                case 55:
                    dificultat += 0.030f; break;
                case 60:
                    dificultat += 0.032f; break;
                case 65:
                    dificultat += 0.034f; break;
                case 70:
                    dificultat += 0.036f; break;
                case 75:
                    dificultat += 0.038f; break;
                case 80:
                    dificultat += 0.04f; break;
                case 85:
                    dificultat += 0.042f; break;
                case 90:
                    dificultat += 0.044f; break;
                case 95:
                    dificultat += 0.046f; break;
                case 100:
                    dificultat += 0.048f; break;
            }
            ColorPrincipal = new Color(0, 0, dificultat, 1);
        }
        if (GameManager.Instance.Nivell == 3)
        {
            switch (GameManager.Instance.Ronda)
            {
                case 10:
                    dificultat += 0.005f; break;
                case 15:
                    dificultat += 0.010f; break;
                case 20:
                    dificultat += 0.015f; break;
                case 25:
                    dificultat += 0.018f; break;
                case 30:
                    dificultat += 0.02f; break;
                case 35:
                    dificultat += 0.022f; break;
                case 40:
                    dificultat += 0.024f; break;
                case 45:
                    dificultat += 0.026f; break;
                case 50:
                    dificultat += 0.028f; break;
                case 55:
                    dificultat += 0.030f; break;
                case 60:
                    dificultat += 0.032f; break;
                case 65:
                    dificultat += 0.034f; break;
                case 70:
                    dificultat += 0.036f; break;
                case 75:
                    dificultat += 0.038f; break;
                case 80:
                    dificultat += 0.04f; break;
                case 85:
                    dificultat += 0.042f; break;
                case 90:
                    dificultat += 0.044f; break;
                case 95:
                    dificultat += 0.046f; break;
                case 100:
                    dificultat += 0.048f; break;
            }
            ColorPrincipal = new Color(0, dificultat, 0, 1);
        }
        imatge.color = ColorPrincipal;
        solucion = false;
        }*/


    public void initSolu() {

        
        dificultatSolucio = Random.Range(0.9f, 1f);
        

        if (GameManager.Instance.Nivell == 0)
        {
            ColorSolucion = new Color(1, 1, 1, dificultatSolucio);
        }
        if (GameManager.Instance.Nivell == 1)
        {
            ColorSolucion = new Color(dificultatSolucio, 0, 0, 1);
        }
        if (GameManager.Instance.Nivell == 2)
        {
            ColorSolucion = new Color(0, 0, dificultatSolucio, 1);            
        }
        if (GameManager.Instance.Nivell == 3)
        {
            ColorSolucion = new Color(0, dificultatSolucio, 0, 1);
        }
        imatge.color = ColorSolucion;
        solucion = true;
    }
    

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
        // Em de cridar la solucio del GameManager i pasar-l'hi la variable solucio
        GameManager.Instance.resposta(solucion);
        }
    }
}
