﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUIController : MonoBehaviour
{
    public Dropdown SelectorNivell;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initMenu();
        SelectorNivell.AddOptions(GameManager.Instance.AvariableLevels);
        /*Nivells:
        "Nivell 0: Blanc/Negre"
        "Nivell 1: Vermell"
        "Nivell 2: Blau"
        "Nivell 3: Verd"
        */
    }
    public void jugar()
    {
        GameManager.Instance.ToPlay(SelectorNivell.value);
    }
    public void resultat()
    {
        GameManager.Instance.ToResults();
    }
    public void sortir()
    {
        Application.Quit();
    }
}
