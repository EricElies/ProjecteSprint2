﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    
    public int Nivells;
    public int Nivell;
    public int[] Puntuacions;
    public int Ronda=0;
    public int Vides = 2;
    public List<Dropdown.OptionData> AvariableLevels;
    public List<Dropdown.OptionData> OtherLevels;
    public MenuUIController UIMenu;
    public PartidaUIController UIPartida;
    public FinalNivellUIController UIFinalNivell;
    public FinalJocUIController UIFinalJoc;
    public AudioManager Audio;

    void Start()
        {
            Puntuacions = new int[Nivells];
        }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    public void iniciNivell()
    {
        if (Nivell == 0)
        {
            Ronda = 100;
        }
        else
        {
            Ronda = 5;
        }
        Vides = 2;
    }
    public void seguentRonda()
    {
        if (Ronda == 100)
        {
            acabarNivell();
        }
        else
        {
            if (Nivell == 0)
            {
                Ronda = 100;
            }
            else
            {
                Ronda += 5;
                SceneManager.LoadScene("Partida");
            }
            
            //Generar nova solució
        }
        
        //UIPartida.actualitzarRonda();
    }
    public void resposta(bool correcte)
    {
        //Cridada des del grid
        //GameManager.Instance.resposta(si es correcte o no);
        if (correcte)
        {
            Audio.PlaySound("Correcte");
            seguentRonda();
        }
        else
        {
            Audio.PlaySound("Incorrecte");
            Vides--;
            if (Vides <= 0)
            {
                if (Nivell == 0)
                {
                    Ronda = 0;
                }
                else
                {
                    Ronda -= 5;
                }
                acabarNivell();
            }
            else
            {
                UIPartida.actualitzarVides();
            }
        }
    }
    public void acabarNivell()
    {
        if (Nivell == 0&&Ronda==100&&AvariableLevels.Count==1)
        {
            foreach (Dropdown.OptionData opcio in OtherLevels)
            {
                AvariableLevels.Add(opcio);
            }
        }
        Puntuacions[Nivell] = Ronda;
        SceneManager.LoadScene("FinalNivell");
        //Acabar el nivell
    }
    public void ToPlay(int nivellAJugar)
    {
        Nivell = nivellAJugar;
        iniciNivell();
        SceneManager.LoadScene("Partida");
    }
    public void ToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void ToResults()
    {
        SceneManager.LoadScene("FinalJoc");
    }
    public void NextLevel()
    {
        Nivell++;
        ToPlay(Nivell);
    }
    public void initFinalNivell()
    {
        UIFinalNivell = GameObject.Find("PuntuacioFinalNivell").GetComponent<FinalNivellUIController>();
    }
    public void initFinalJoc()
    {
        UIFinalJoc = GameObject.Find("PuntuacioFinalCanvas").GetComponent<FinalJocUIController>();
    }
    public void initMenu ()
    {
        UIMenu = GameObject.Find("Menu").GetComponent<MenuUIController>();
    }
    public void initPartida()
    {
        Audio = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        UIPartida = GameObject.Find("UIGameplay").GetComponent<PartidaUIController>();
    }
}
