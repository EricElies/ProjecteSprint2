﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCycle : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Color[] color;
    private int index;
    public float speed;
    private float changer;

        
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    
    void Update()
    {
        spriteRenderer.material.color = Color.Lerp(spriteRenderer.material.color, color[index], speed*Time.deltaTime);
        changer = Mathf.Lerp(changer, 1f, speed * Time.deltaTime);

        if(changer>0.9f)
        {
            changer = 0;
            index++;

            index = (index > color.Length) ? 0 : index;

        }
    }

}
