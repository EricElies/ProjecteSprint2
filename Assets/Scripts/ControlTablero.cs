﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTablero : MonoBehaviour
{
    [SerializeField] public Celda celda;
    [SerializeField] public int Tamany = 20;
    [SerializeField] Camera cam;


    

    private void Start()
    {
        GenerarGrid();
        
        SpawnSolution();
    }


    public void GenerarGrid() {

        for (int i = 0; i < Tamany; i++)
        {
            for (int j = 0; j < Tamany; j++)
            {
                celda.init();
                var spawnCelda = Instantiate(celda, new Vector3(i, j), Quaternion.identity);
                spawnCelda.name = $"Celda{ i}{ j}";
                
                
            }
        }
        cam.transform.position = new Vector3((float) Tamany/2 - 1f, (float)Tamany / 2 + 1f,-10);
    }

    public void SpawnSolution()
    {
        int x = Random.Range(0, Tamany - 2);
        int y = Random.Range(0, Tamany - 2);

        for (int t = 0; t < 2; t++)
        {
            for (int r = 0; r < 2; r++)
            {
                celda.initSolu();
                var spawnCelda = Instantiate(celda, new Vector3(x + t, y + r), Quaternion.identity);
                spawnCelda.name = $"Celda{x+t}{y+r}";
                
            }
        }
    }
}
