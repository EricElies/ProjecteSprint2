﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartidaUIController : MonoBehaviour
{
    public Text NivellActual;
    public Text RondaActual;
    public Text VidesActuals;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initPartida();
        actualitzarNivell();
        actualitzarRonda();
        actualitzarVides();
    }
    public void retornMenu()
    {
        GameManager.Instance.ToMenu();
    }
    public void actualitzarNivell()
    {
        switch (GameManager.Instance.Nivell)
        {
            case 0:
                NivellActual.text = "Nivell 0\nBlanc/Negre";
                break;
            case 1:
                NivellActual.text = "Nivell 1\nVermell";
                break;
            case 2:
                NivellActual.text = "Nivell 2\nBlau";
                break;
            case 3:
                NivellActual.text = "Nivell 3\nVerd";
                break;
        }
    }
    public void actualitzarRonda()
    {
        RondaActual.text = (GameManager.Instance.Ronda.ToString()+"%");
    }
    public void actualitzarVides()
    {
        VidesActuals.text = ("Vides: "+GameManager.Instance.Vides);
    }
}
