﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalJocUIController : MonoBehaviour
{
    public Text[] Nivells;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.initFinalJoc();
        for (int i = 0; i < Nivells.Length;i++)
        {
            Nivells[i].text = (Nivells[i].name+": "+ GameManager.Instance.Puntuacions[i]+"%");
        }
    }
    public void retornMenu()
    {
        GameManager.Instance.ToMenu();
    }
}
